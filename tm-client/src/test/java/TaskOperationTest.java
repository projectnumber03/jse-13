import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.shilov.tm.api.endpoint.Role;
import ru.shilov.tm.api.endpoint.Status;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.api.endpoint.User;
import ru.shilov.tm.context.Bootstrap;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TaskOperationTest {

    class TaskFactory {

        private int count = 1;

        private Task createTask() {
            @NotNull final Task p = new Task();
            p.setName("task" + count);
            p.setDescription("description" + count);
            p.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()));
            p.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(1)));
            p.setStatus(Status.PLANNED);
            count++;
            return p;
        }

        private void createTasks(@NotNull final Bootstrap b, final int count) throws Exception {
            for (int i = 0; i < count; i++) {
                b.getTaskEndPoint().persistTask(b.getToken(), createTask());
            }
        }

    }

    @Before
    public void registerUser() throws Exception {
        @NotNull final Bootstrap b = new Bootstrap();
        @NotNull final User testUser = new User();
        testUser.setLogin("test");
        testUser.setPassword("test");
        testUser.setRole(Role.ADMIN);
        b.getUserEndPoint().registerUser(testUser);
    }

    @After
    public void deleteUser() throws Exception {
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        @Nullable final String idToDelete = b.getUserEndPoint().findAllUsers(b.getToken()).stream()
                .filter(u -> "test".equals(u.getLogin()))
                .map(User::getId).findAny().orElse(null);
        b.getUserEndPoint().removeOneUserById(b.getToken(), idToDelete);
    }

    @Test
    public void clearTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        tf.createTasks(b, 10);
        @NotNull List<Task> tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(10, tasks.size());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(0, tasks.size());
    }

    @Test
    public void findAllByUserIdTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tf.createTasks(b, 10);
        @NotNull List<Task> tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(10, tasks.size());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void findByNameOrDescriptionTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tf.createTasks(b, 10);
        @NotNull List<Task> tasks = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task1");
        assertEquals(2, tasks.size());
        tasks = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "description1");
        assertEquals(2, tasks.size());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void findOneTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tf.createTasks(b, 10);
        @Nullable final Task taskToFind = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task2").stream().findAny().orElse(null);
        assertNotNull(taskToFind);
        @NotNull final Task project = b.getTaskEndPoint().findOneTask(b.getToken(), taskToFind.getId());
        assertEquals(project.getId(), taskToFind.getId());
        assertEquals(project.getName(), taskToFind.getName());
        assertEquals(project.getDescription(), taskToFind.getDescription());
        assertEquals(project.getStart(), taskToFind.getStart());
        assertEquals(project.getFinish(), taskToFind.getFinish());
        assertEquals(project.getUserId(), taskToFind.getUserId());
        assertEquals(project.getStatus(), taskToFind.getStatus());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void mergeTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tf.createTasks(b, 10);
        @Nullable final Task taskToMerge = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task1").stream().findAny().orElse(null);
        assertNotNull(taskToMerge);
        taskToMerge.setName("task-1");
        taskToMerge.setDescription("description-1");
        taskToMerge.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(2)));
        taskToMerge.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(3)));
        taskToMerge.setStatus(Status.IN_PROCESS);
        b.getTaskEndPoint().mergeTask(b.getToken(), taskToMerge);
        @NotNull final List<Task> empty = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "test-task1");
        assertEquals(0, empty.size());
        @Nullable final Task task = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task-1").stream().findAny().orElse(null);
        assertNotNull(task);
        assertEquals(task.getName(), "task-1");
        assertEquals(task.getDescription(), "description-1");
        assertEquals(task.getStart(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(2)));
        assertEquals(task.getFinish(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(3)));
        assertEquals(task.getStatus(), Status.IN_PROCESS);
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void persistTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        b.getTaskEndPoint().persistTask(b.getToken(), tf.createTask());
        @Nullable final Task task = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task1").stream().findAny().orElse(null);
        assertNotNull(task);
        assertEquals(task.getName(), "task1");
        assertEquals(task.getDescription(), "description1");
        assertEquals(task.getStart(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()));
        assertEquals(task.getFinish(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(1)));
        assertEquals(task.getStatus(), Status.PLANNED);
        @NotNull final List<Task> tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(1, tasks.size());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void removeTest() throws Exception {
        @NotNull final TaskFactory pf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        pf.createTasks(b, 10);
        @Nullable final Task taskToDelete = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task2").stream().findAny().orElse(null);
        assertNotNull(taskToDelete);
        b.getTaskEndPoint().removeOneTaskByUserId(b.getToken(), taskToDelete.getId());
        @NotNull final List<Task> tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(9, tasks.size());
        assertFalse(tasks.stream().map(Task::getName).anyMatch("task2"::equals));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

}
