package ru.shilov.tm.command.load;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class EntityJaxBJsonLoadCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        getEndPointLocator().getDataTransportEndPoint().loadDataJsonByJaxB(getToken());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "load-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузка предметной области из json с использованием JAXB";
    }

}
