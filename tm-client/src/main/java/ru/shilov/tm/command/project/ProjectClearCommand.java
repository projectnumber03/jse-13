package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class ProjectClearCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        getEndPointLocator().getProjectEndPoint().removeAllProjects(token);
        getEndPointLocator().getTaskEndPoint().removeAllTasks(token);
        System.out.println("[ПРОЕКТЫ УДАЛЕНЫ]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление всех проектов";
    }

}
