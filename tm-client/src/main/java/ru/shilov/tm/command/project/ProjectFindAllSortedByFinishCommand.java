package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Project;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class ProjectFindAllSortedByFinishCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final List<Project> projects = getEndPointLocator().getProjectEndPoint().findProjectsByUserId(getToken()).stream()
                .filter(p -> p.getFinish() != null)
                .sorted(Comparator.comparing(Project::getFinish))
                .collect(Collectors.toList());
        getServiceLocator().getTerminalService().printAllProjects(projects);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list-finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список проектов по дате окончания";
    }

}
