package ru.shilov.tm.command.project;

import ru.shilov.tm.api.endpoint.Project;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class ProjectFindOneCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getEndPointLocator().getProjectEndPoint().getProjectId(token, getServiceLocator().getTerminalService().nextLine());
        @NotNull final Project project = getEndPointLocator().getProjectEndPoint().findOneProject(token, projectId);
        @NotNull final List<Task> tasks = getEndPointLocator().getTaskEndPoint().findTasksByProjectId(token, projectId);
        System.out.println(project.getName());
        getServiceLocator().getTerminalService().printAllTasks(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-tasks";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список задач в проекте";
    }

}
