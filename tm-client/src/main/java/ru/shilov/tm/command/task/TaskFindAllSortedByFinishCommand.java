package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskFindAllSortedByFinishCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final List<Task> tasks = getEndPointLocator().getTaskEndPoint().findTasksByUserId(getToken()).stream()
                .filter(t -> t.getFinish() != null)
                .sorted(Comparator.comparing(Task::getFinish))
                .collect(Collectors.toList());
        getServiceLocator().getTerminalService().printAllTasks(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list-finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список задач по дате окончания";
    }

}
