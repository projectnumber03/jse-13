package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class TaskAttachCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskId = getEndPointLocator().getTaskEndPoint().getTaskId(token, getServiceLocator().getTerminalService().nextLine());
        @NotNull final Task t = getEndPointLocator().getTaskEndPoint().findOneTask(token, taskId);
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getEndPointLocator().getProjectEndPoint().getProjectId(token, getServiceLocator().getTerminalService().nextLine());
        t.setProjectId(projectId);
        getEndPointLocator().getTaskEndPoint().mergeTask(token, t);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-attach";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Добавление задачи в проект";
    }

}
