package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class ProjectRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getEndPointLocator().getProjectEndPoint().getProjectId(token, getServiceLocator().getTerminalService().nextLine());
        getEndPointLocator().getProjectEndPoint().removeOneProjectByUserId(token, projectId);
        System.out.println("[ПРОЕКТ УДАЛЕН]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление проекта";
    }

}
