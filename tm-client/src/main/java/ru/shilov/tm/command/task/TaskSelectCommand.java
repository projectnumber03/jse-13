package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class TaskSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskId = getEndPointLocator().getTaskEndPoint().getTaskId(token, getServiceLocator().getTerminalService().nextLine());
        @NotNull final Task t = getEndPointLocator().getTaskEndPoint().findOneTask(token, taskId);
        getServiceLocator().getTerminalService().printTaskProperties(t);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Свойства задачи";
    }

}
