package ru.shilov.tm.command.save;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class EntityFasterJsonSaveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        getEndPointLocator().getDataTransportEndPoint().saveDataJsonByFasterXml(getToken());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save-json-f";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области в json с использованием FasterXml";
    }

}
