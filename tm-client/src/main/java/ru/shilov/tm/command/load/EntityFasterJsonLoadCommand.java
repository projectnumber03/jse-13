package ru.shilov.tm.command.load;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class EntityFasterJsonLoadCommand extends AbstractTerminalCommand {

    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        getEndPointLocator().getDataTransportEndPoint().loadDataJsonByFasterXml(getToken());
        System.out.println("[OK]");
    }

    @NotNull
    public String getName() {
        return "load-json-f";
    }

    @NotNull
    public String getDescription() {
        return "Загрузка предметной области из json с использованием FasterXml";
    }

}
