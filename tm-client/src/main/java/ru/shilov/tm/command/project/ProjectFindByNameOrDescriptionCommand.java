package ru.shilov.tm.command.project;

import ru.shilov.tm.api.endpoint.Project;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class ProjectFindByNameOrDescriptionCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ФРАЗУ ДЛЯ ПОИСКА:");
        @NotNull final String searchPhrase = getServiceLocator().getTerminalService().nextLine();
        @NotNull final List<Project> projects = getEndPointLocator().getProjectEndPoint().findProjectsByNameOrDescription(token, searchPhrase);
        getServiceLocator().getTerminalService().printAllProjects(projects);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск проектов";
    }

}
