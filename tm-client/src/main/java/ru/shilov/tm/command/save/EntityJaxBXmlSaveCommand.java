package ru.shilov.tm.command.save;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class EntityJaxBXmlSaveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        getEndPointLocator().getDataTransportEndPoint().saveDataXmlByJaxB(getToken());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области в xml с использованием JAXB";
    }

}
