package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Project;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public final class ProjectPersistCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        @NotNull final Project p = new Project();
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ПРОЕКТА:");
        p.setName(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        p.setDescription(getServiceLocator().getTerminalService().nextLine());
        try {
            @NotNull final DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            @NotNull final DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            p.setStart(outputFormatter.format(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), inputFormatter)));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            p.setFinish(outputFormatter.format(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), inputFormatter)));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        getEndPointLocator().getProjectEndPoint().persistProject(token, p);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создание проекта";
    }

}
