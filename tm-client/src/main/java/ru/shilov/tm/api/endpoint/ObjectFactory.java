
package ru.shilov.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.shilov.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "Exception");
    private final static QName _LoadDataBin_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataBin");
    private final static QName _LoadDataBinResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataBinResponse");
    private final static QName _LoadDataJsonByFasterXml_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataJsonByFasterXml");
    private final static QName _LoadDataJsonByFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataJsonByFasterXmlResponse");
    private final static QName _LoadDataJsonByJaxB_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataJsonByJaxB");
    private final static QName _LoadDataJsonByJaxBResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataJsonByJaxBResponse");
    private final static QName _LoadDataXmlByFasterXml_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataXmlByFasterXml");
    private final static QName _LoadDataXmlByFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataXmlByFasterXmlResponse");
    private final static QName _LoadDataXmlByJaxB_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataXmlByJaxB");
    private final static QName _LoadDataXmlByJaxBResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "loadDataXmlByJaxBResponse");
    private final static QName _SaveDataBin_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataBin");
    private final static QName _SaveDataBinResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataBinResponse");
    private final static QName _SaveDataJsonByFasterXml_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataJsonByFasterXml");
    private final static QName _SaveDataJsonByFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataJsonByFasterXmlResponse");
    private final static QName _SaveDataJsonByJaxB_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataJsonByJaxB");
    private final static QName _SaveDataJsonByJaxBResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataJsonByJaxBResponse");
    private final static QName _SaveDataXmlByFasterXml_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataXmlByFasterXml");
    private final static QName _SaveDataXmlByFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataXmlByFasterXmlResponse");
    private final static QName _SaveDataXmlByJaxB_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataXmlByJaxB");
    private final static QName _SaveDataXmlByJaxBResponse_QNAME = new QName("http://endpoint.api.tm.shilov.ru/", "saveDataXmlByJaxBResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.shilov.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link LoadDataBin }
     * 
     */
    public LoadDataBin createLoadDataBin() {
        return new LoadDataBin();
    }

    /**
     * Create an instance of {@link LoadDataBinResponse }
     * 
     */
    public LoadDataBinResponse createLoadDataBinResponse() {
        return new LoadDataBinResponse();
    }

    /**
     * Create an instance of {@link LoadDataJsonByFasterXml }
     * 
     */
    public LoadDataJsonByFasterXml createLoadDataJsonByFasterXml() {
        return new LoadDataJsonByFasterXml();
    }

    /**
     * Create an instance of {@link LoadDataJsonByFasterXmlResponse }
     * 
     */
    public LoadDataJsonByFasterXmlResponse createLoadDataJsonByFasterXmlResponse() {
        return new LoadDataJsonByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link LoadDataJsonByJaxB }
     * 
     */
    public LoadDataJsonByJaxB createLoadDataJsonByJaxB() {
        return new LoadDataJsonByJaxB();
    }

    /**
     * Create an instance of {@link LoadDataJsonByJaxBResponse }
     * 
     */
    public LoadDataJsonByJaxBResponse createLoadDataJsonByJaxBResponse() {
        return new LoadDataJsonByJaxBResponse();
    }

    /**
     * Create an instance of {@link LoadDataXmlByFasterXml }
     * 
     */
    public LoadDataXmlByFasterXml createLoadDataXmlByFasterXml() {
        return new LoadDataXmlByFasterXml();
    }

    /**
     * Create an instance of {@link LoadDataXmlByFasterXmlResponse }
     * 
     */
    public LoadDataXmlByFasterXmlResponse createLoadDataXmlByFasterXmlResponse() {
        return new LoadDataXmlByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link LoadDataXmlByJaxB }
     * 
     */
    public LoadDataXmlByJaxB createLoadDataXmlByJaxB() {
        return new LoadDataXmlByJaxB();
    }

    /**
     * Create an instance of {@link LoadDataXmlByJaxBResponse }
     * 
     */
    public LoadDataXmlByJaxBResponse createLoadDataXmlByJaxBResponse() {
        return new LoadDataXmlByJaxBResponse();
    }

    /**
     * Create an instance of {@link SaveDataBin }
     * 
     */
    public SaveDataBin createSaveDataBin() {
        return new SaveDataBin();
    }

    /**
     * Create an instance of {@link SaveDataBinResponse }
     * 
     */
    public SaveDataBinResponse createSaveDataBinResponse() {
        return new SaveDataBinResponse();
    }

    /**
     * Create an instance of {@link SaveDataJsonByFasterXml }
     * 
     */
    public SaveDataJsonByFasterXml createSaveDataJsonByFasterXml() {
        return new SaveDataJsonByFasterXml();
    }

    /**
     * Create an instance of {@link SaveDataJsonByFasterXmlResponse }
     * 
     */
    public SaveDataJsonByFasterXmlResponse createSaveDataJsonByFasterXmlResponse() {
        return new SaveDataJsonByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link SaveDataJsonByJaxB }
     * 
     */
    public SaveDataJsonByJaxB createSaveDataJsonByJaxB() {
        return new SaveDataJsonByJaxB();
    }

    /**
     * Create an instance of {@link SaveDataJsonByJaxBResponse }
     * 
     */
    public SaveDataJsonByJaxBResponse createSaveDataJsonByJaxBResponse() {
        return new SaveDataJsonByJaxBResponse();
    }

    /**
     * Create an instance of {@link SaveDataXmlByFasterXml }
     * 
     */
    public SaveDataXmlByFasterXml createSaveDataXmlByFasterXml() {
        return new SaveDataXmlByFasterXml();
    }

    /**
     * Create an instance of {@link SaveDataXmlByFasterXmlResponse }
     * 
     */
    public SaveDataXmlByFasterXmlResponse createSaveDataXmlByFasterXmlResponse() {
        return new SaveDataXmlByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link SaveDataXmlByJaxB }
     * 
     */
    public SaveDataXmlByJaxB createSaveDataXmlByJaxB() {
        return new SaveDataXmlByJaxB();
    }

    /**
     * Create an instance of {@link SaveDataXmlByJaxBResponse }
     * 
     */
    public SaveDataXmlByJaxBResponse createSaveDataXmlByJaxBResponse() {
        return new SaveDataXmlByJaxBResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataBin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataBin")
    public JAXBElement<LoadDataBin> createLoadDataBin(LoadDataBin value) {
        return new JAXBElement<LoadDataBin>(_LoadDataBin_QNAME, LoadDataBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBinResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataBinResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataBinResponse")
    public JAXBElement<LoadDataBinResponse> createLoadDataBinResponse(LoadDataBinResponse value) {
        return new JAXBElement<LoadDataBinResponse>(_LoadDataBinResponse_QNAME, LoadDataBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonByFasterXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataJsonByFasterXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataJsonByFasterXml")
    public JAXBElement<LoadDataJsonByFasterXml> createLoadDataJsonByFasterXml(LoadDataJsonByFasterXml value) {
        return new JAXBElement<LoadDataJsonByFasterXml>(_LoadDataJsonByFasterXml_QNAME, LoadDataJsonByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonByFasterXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataJsonByFasterXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataJsonByFasterXmlResponse")
    public JAXBElement<LoadDataJsonByFasterXmlResponse> createLoadDataJsonByFasterXmlResponse(LoadDataJsonByFasterXmlResponse value) {
        return new JAXBElement<LoadDataJsonByFasterXmlResponse>(_LoadDataJsonByFasterXmlResponse_QNAME, LoadDataJsonByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonByJaxB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataJsonByJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataJsonByJaxB")
    public JAXBElement<LoadDataJsonByJaxB> createLoadDataJsonByJaxB(LoadDataJsonByJaxB value) {
        return new JAXBElement<LoadDataJsonByJaxB>(_LoadDataJsonByJaxB_QNAME, LoadDataJsonByJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonByJaxBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataJsonByJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataJsonByJaxBResponse")
    public JAXBElement<LoadDataJsonByJaxBResponse> createLoadDataJsonByJaxBResponse(LoadDataJsonByJaxBResponse value) {
        return new JAXBElement<LoadDataJsonByJaxBResponse>(_LoadDataJsonByJaxBResponse_QNAME, LoadDataJsonByJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlByFasterXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataXmlByFasterXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataXmlByFasterXml")
    public JAXBElement<LoadDataXmlByFasterXml> createLoadDataXmlByFasterXml(LoadDataXmlByFasterXml value) {
        return new JAXBElement<LoadDataXmlByFasterXml>(_LoadDataXmlByFasterXml_QNAME, LoadDataXmlByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlByFasterXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataXmlByFasterXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataXmlByFasterXmlResponse")
    public JAXBElement<LoadDataXmlByFasterXmlResponse> createLoadDataXmlByFasterXmlResponse(LoadDataXmlByFasterXmlResponse value) {
        return new JAXBElement<LoadDataXmlByFasterXmlResponse>(_LoadDataXmlByFasterXmlResponse_QNAME, LoadDataXmlByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlByJaxB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataXmlByJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataXmlByJaxB")
    public JAXBElement<LoadDataXmlByJaxB> createLoadDataXmlByJaxB(LoadDataXmlByJaxB value) {
        return new JAXBElement<LoadDataXmlByJaxB>(_LoadDataXmlByJaxB_QNAME, LoadDataXmlByJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlByJaxBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDataXmlByJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "loadDataXmlByJaxBResponse")
    public JAXBElement<LoadDataXmlByJaxBResponse> createLoadDataXmlByJaxBResponse(LoadDataXmlByJaxBResponse value) {
        return new JAXBElement<LoadDataXmlByJaxBResponse>(_LoadDataXmlByJaxBResponse_QNAME, LoadDataXmlByJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataBin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataBin")
    public JAXBElement<SaveDataBin> createSaveDataBin(SaveDataBin value) {
        return new JAXBElement<SaveDataBin>(_SaveDataBin_QNAME, SaveDataBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBinResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataBinResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataBinResponse")
    public JAXBElement<SaveDataBinResponse> createSaveDataBinResponse(SaveDataBinResponse value) {
        return new JAXBElement<SaveDataBinResponse>(_SaveDataBinResponse_QNAME, SaveDataBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonByFasterXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataJsonByFasterXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataJsonByFasterXml")
    public JAXBElement<SaveDataJsonByFasterXml> createSaveDataJsonByFasterXml(SaveDataJsonByFasterXml value) {
        return new JAXBElement<SaveDataJsonByFasterXml>(_SaveDataJsonByFasterXml_QNAME, SaveDataJsonByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonByFasterXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataJsonByFasterXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataJsonByFasterXmlResponse")
    public JAXBElement<SaveDataJsonByFasterXmlResponse> createSaveDataJsonByFasterXmlResponse(SaveDataJsonByFasterXmlResponse value) {
        return new JAXBElement<SaveDataJsonByFasterXmlResponse>(_SaveDataJsonByFasterXmlResponse_QNAME, SaveDataJsonByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonByJaxB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataJsonByJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataJsonByJaxB")
    public JAXBElement<SaveDataJsonByJaxB> createSaveDataJsonByJaxB(SaveDataJsonByJaxB value) {
        return new JAXBElement<SaveDataJsonByJaxB>(_SaveDataJsonByJaxB_QNAME, SaveDataJsonByJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonByJaxBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataJsonByJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataJsonByJaxBResponse")
    public JAXBElement<SaveDataJsonByJaxBResponse> createSaveDataJsonByJaxBResponse(SaveDataJsonByJaxBResponse value) {
        return new JAXBElement<SaveDataJsonByJaxBResponse>(_SaveDataJsonByJaxBResponse_QNAME, SaveDataJsonByJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlByFasterXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataXmlByFasterXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataXmlByFasterXml")
    public JAXBElement<SaveDataXmlByFasterXml> createSaveDataXmlByFasterXml(SaveDataXmlByFasterXml value) {
        return new JAXBElement<SaveDataXmlByFasterXml>(_SaveDataXmlByFasterXml_QNAME, SaveDataXmlByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlByFasterXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataXmlByFasterXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataXmlByFasterXmlResponse")
    public JAXBElement<SaveDataXmlByFasterXmlResponse> createSaveDataXmlByFasterXmlResponse(SaveDataXmlByFasterXmlResponse value) {
        return new JAXBElement<SaveDataXmlByFasterXmlResponse>(_SaveDataXmlByFasterXmlResponse_QNAME, SaveDataXmlByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlByJaxB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataXmlByJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataXmlByJaxB")
    public JAXBElement<SaveDataXmlByJaxB> createSaveDataXmlByJaxB(SaveDataXmlByJaxB value) {
        return new JAXBElement<SaveDataXmlByJaxB>(_SaveDataXmlByJaxB_QNAME, SaveDataXmlByJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlByJaxBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDataXmlByJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.shilov.ru/", name = "saveDataXmlByJaxBResponse")
    public JAXBElement<SaveDataXmlByJaxBResponse> createSaveDataXmlByJaxBResponse(SaveDataXmlByJaxBResponse value) {
        return new JAXBElement<SaveDataXmlByJaxBResponse>(_SaveDataXmlByJaxBResponse_QNAME, SaveDataXmlByJaxBResponse.class, null, value);
    }

}
