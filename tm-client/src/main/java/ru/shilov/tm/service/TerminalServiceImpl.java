package ru.shilov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.AbstractEntity;
import ru.shilov.tm.api.endpoint.Project;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.api.endpoint.User;
import ru.shilov.tm.api.service.ITerminalService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public final class TerminalServiceImpl implements ITerminalService {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @NotNull
    public String nextLine() {
        return scanner.nextLine();
    }

    @Override
    public void printAllProjects(@NotNull final List<Project> projects) {
        for (int i = 1; i <= projects.size(); i++) {
            System.out.println(String.format("%d. %s", i, projects.get(i - 1).getName()));
        }
    }

    @Override
    public void printAllTasks(@NotNull final List<Task> tasks) {
        for (int i = 1; i <= tasks.size(); i++) {
            System.out.println(String.format("%d. %s", i, tasks.get(i - 1).getName()));
        }
    }

    @Override
    public void printAllUsers(@NotNull final List<User> users) {
        for (int i = 1; i <= users.size(); i++) {
            System.out.println(String.format("%d. %s", i, users.get(i - 1).getLogin()));
        }
    }

    @Override
    public void printProjectProperties(@NotNull final Project p) {
        @NotNull final StringBuilder sb = new StringBuilder();
        sb.append("Проект: ").append(p.getName()).append("\n");
        sb.append("Описание: ").append(p.getDescription()).append("\n");
        sb.append("Дата начала: ").append(DateTimeFormatter.ofPattern("dd.MM.yyyy").format(LocalDate.parse(p.getStart()))).append("\n");
        sb.append("Дата окончания: ").append(DateTimeFormatter.ofPattern("dd.MM.yyyy").format(LocalDate.parse(p.getFinish()))).append("\n");
        sb.append("Статус: ").append(p.getStatus().value());
        System.out.println(sb.toString());;
    }

    @Override
    public void printTaskProperties(@NotNull final Task t) {
        @NotNull final StringBuilder sb = new StringBuilder();
        sb.append("Задача: ").append(t.getName()).append("\n");
        sb.append("Описание: ").append(t.getDescription()).append("\n");
        sb.append("Дата начала: ").append(DateTimeFormatter.ofPattern("dd.MM.yyyy").format(LocalDate.parse(t.getStart()))).append("\n");
        sb.append("Дата окончания: ").append(DateTimeFormatter.ofPattern("dd.MM.yyyy").format(LocalDate.parse(t.getFinish()))).append("\n");
        sb.append("Статус: ").append(t.getDescription());
        System.out.println(sb.toString());
    }

    @Override
    public void printUserProperties(@NotNull final User u) {
        @NotNull final StringBuilder sb = new StringBuilder();
        sb.append("Пользователь: ").append(u.getLogin()).append("\n");
        sb.append("Роль: ").append(u.getRole() != null ? u.getRole().value() : "n/a");
        System.out.println(sb.toString());
    }

}
