package ru.shilov.tm.error;

public final class IllegalCommandException extends Exception {

    public IllegalCommandException() {
        super("Неверная команда");
    }

}
