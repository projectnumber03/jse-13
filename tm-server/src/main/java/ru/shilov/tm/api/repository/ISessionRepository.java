package ru.shilov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    @Select("SELECT * FROM app_session WHERE userId = #{userId} AND id = #{id}")
    Session findOneByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws SQLException;

    @Delete("DELETE FROM app_session WHERE userId = #{userId}")
    void removeByUserId(@NotNull @Param("userId") final String userId) throws SQLException;

    @Override
    @Select("SELECT * FROM app_session")
    @NotNull List<Session> findAll() throws SQLException;

    @Nullable
    @Override
    @Select("SELECT * FROM app_session WHERE id = #{id}")
    Session findOne(@NotNull @Param("id") final String id) throws SQLException;

    @Override
    @Delete("DELETE FROM app_session WHERE id IN (SELECT id FROM app_session)")
    void removeAll() throws SQLException;

    @Override
    @Insert("INSERT INTO app_session VALUES(#{id}, #{signature}, #{creationDate}, #{userId}, #{role})")
    void persist(@NotNull final Session entity) throws SQLException;

}
