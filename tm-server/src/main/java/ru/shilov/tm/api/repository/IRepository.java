package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface IRepository<T> {

    @NotNull
    List<T> findAll() throws SQLException;

    @Nullable
    T findOne(@NotNull final String id) throws SQLException;

    void removeAll() throws SQLException;

    void persist(@NotNull final T entity) throws SQLException;

    void merge(@NotNull final T entity) throws SQLException;

}
