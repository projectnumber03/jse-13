package ru.shilov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    @Select("SELECT * FROM app_task where userId = #{userId} AND projectId = #{projectId}")
    List<Task> findByProjectId(@NotNull @Param("userId") final String userId, @NotNull @Param("projectId") final String projectId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} AND (description LIKE #{value} OR name LIKE #{value})")
    List<Task> findByNameOrDescription(@NotNull @Param("userId") final String userId, @NotNull @Param("value") final String value) throws SQLException;

    @NotNull
    @Select("SELECT id FROM app_task WHERE userId = #{userId} LIMIT 1 OFFSET #{value}")
    String getId(@NotNull @Param("userId") final String userId, @NotNull @Param("value") final Integer value) throws SQLException;

    @NotNull
    @Delete("DELETE FROM app_task WHERE userId = #{userId}")
    Boolean removeByUserId(@NotNull @Param("userId") final String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId}")
    List<Task> findByUserId(@NotNull final String userId) throws SQLException;

    @Delete("DELETE FROM app_task WHERE userId = #{userId} AND id = #{id}")
    void removeOneByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws SQLException;

    @NotNull
    @Override
    @Select("SELECT * FROM app_task")
    List<Task> findAll() throws SQLException;

    @Nullable
    @Override
    @Select("SELECT * FROM app_task WHERE id = #{id}")
    Task findOne(@NotNull @Param("id") final String id) throws SQLException;

    @Override
    @Delete("DELETE FROM app_task WHERE id IN (SELECT id FROM app_task)")
    void removeAll() throws SQLException;

    @Override
    @Insert("INSERT INTO app_task VALUES (#{id}, #{userId}, #{projectId}, #{name}, #{description}, #{status}, #{start}, #{finish})")
    void persist(@NotNull final Task entity) throws SQLException;

    @Override
    @Update("UPDATE app_task SET name = #{name}, description = #{description}, projectId = #{projectId}, start = #{start}, finish = #{finish}, status = #{status} WHERE id = #{id}")
    void merge(@NotNull final Task entity) throws SQLException;

}
