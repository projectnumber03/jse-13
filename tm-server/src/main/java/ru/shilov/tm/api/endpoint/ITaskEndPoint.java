package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndPoint {

    @NotNull
    @WebMethod
    List<Task> findAllTasks(@Nullable final String String) throws Exception;

    @NotNull
    @WebMethod
    Task findOneTask(@Nullable final String String, @NotNull final String id) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findTasksByUserId(@Nullable final String String) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findTasksByProjectId(@Nullable final String String, @NotNull final String id) throws Exception;

    @WebMethod
    void removeAllTasks(@Nullable final String String) throws Exception;

    @WebMethod
    void removeTasksByUserId(@Nullable final String String) throws Exception;

    @WebMethod
    void removeOneTaskByUserId(@Nullable final String String, @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    Task persistTask(@Nullable final String String, @Nullable final Task t) throws Exception;

    @Nullable
    @WebMethod
    Task mergeTask(@Nullable final String String, @NotNull final Task t) throws Exception;

    @NotNull
    @WebMethod
    String getTaskId(@Nullable final String String, @NotNull final String value) throws Exception;

    @NotNull
    List<Task> findTasksByNameOrDescription(@Nullable final String String, @NotNull final String value) throws Exception;

}
