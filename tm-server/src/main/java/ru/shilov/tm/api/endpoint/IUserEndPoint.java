package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public interface IUserEndPoint {

    @NotNull
    @WebMethod
    List<User> findAllUsers(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    User findOneUser(@Nullable final String token) throws Exception;@NotNull

    @WebMethod
    User findOneUserById(@Nullable final String token, @Nullable final String userId) throws Exception;

    @WebMethod
    void removeAllUsers(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    User persistUser(@Nullable final String token, @Nullable final User u) throws Exception;

    @Nullable
    @WebMethod
    User mergeUser(@Nullable final String token, @NotNull final User u) throws Exception;

    @WebMethod
    void removeOneUserById(@Nullable final String token, @Nullable final String id) throws Exception;

    @NotNull
    @WebMethod
    String getUserId(@Nullable final String token, @NotNull final String value) throws Exception;

    @Nullable
    @WebMethod
    User registerUser(@Nullable final User u) throws Exception;

}
