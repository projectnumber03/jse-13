package ru.shilov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    String PERSIST_QUERY = "INSERT INTO app_user VALUES (?, ?, ?, ?)";

    @NotNull
    String MERGE_QUERY = "UPDATE app_user SET login = ?, password = ?, role = ? WHERE id = ?";

    @NotNull
    @Select("SELECT id FROM app_user LIMIT 1 OFFSET #{value}")
    String getId(@NotNull @Param("value") final Integer value) throws SQLException;

    @Delete("DELETE FROM app_user WHERE id = #{id}")
    void removeOneById(@NotNull @Param("id") final String id) throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_user WHERE login = #{login}")
    User findByLogin(@NotNull @Param("login") final String login) throws SQLException;

    @Override
    @Select("SELECT * FROM app_user")
    @NotNull List<User> findAll() throws SQLException;

    @Nullable
    @Override
    @Select("SELECT * FROM app_user WHERE id = #{id}")
    User findOne(@NotNull @Param("id") final String id) throws SQLException;

    @Override
    @Delete("DELETE FROM app_user WHERE id IN (SELECT id FROM app_user)")
    void removeAll() throws SQLException;

    @Override
    @Insert("INSERT INTO app_user VALUES (#{id}, #{login}, #{password}, #{role})")
    void persist(@NotNull final User entity) throws SQLException;

    @Override
    @Update("UPDATE app_user SET login = #{login}, password = #{password}, role = #{role} WHERE id = #{id}")
    void merge(@NotNull final User entity) throws SQLException;

}
