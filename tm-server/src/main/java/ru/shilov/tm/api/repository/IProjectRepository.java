package ru.shilov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Project;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    @Select("SELECT id FROM app_project WHERE userId = #{userId} LIMIT 1 OFFSET #{value}")
    String getId(@NotNull @Param("userId") final String userId, @NotNull @Param("value") final Integer value) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} AND (description LIKE #{value} OR name LIKE #{value})")
    List<Project> findByNameOrDescription(@NotNull @Param("userId") final String userId, @NotNull @Param("value") final String value) throws SQLException;

    @NotNull
    @Delete("DELETE FROM app_project WHERE userId = #{userId}")
    Boolean removeByUserId(@NotNull @Param("userId") final String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId}")
    List<Project> findByUserId(@NotNull @Param("userId") final String userId) throws SQLException;

    @Delete("DELETE FROM app_project WHERE userId = #{userId} AND id = #{id}")
    void removeOneByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws SQLException;

    @NotNull
    @Override
    @Select("SELECT * FROM app_project")
    List<Project> findAll() throws SQLException;

    @NotNull
    @Override
    @Select("SELECT * FROM app_project WHERE id = #{id}")
    Project findOne(@NotNull @Param("id") final String id) throws SQLException;

    @Override
    @Select("DELETE FROM app_project WHERE id IN (SELECT id FROM app_project)")
    void removeAll() throws SQLException;

    @Override
    @Insert("INSERT INTO app_project VALUES (#{id}, #{userId}, #{name}, #{description}, #{status}, #{start}, #{finish})")
    void persist(@NotNull final Project entity) throws SQLException;

    @Override
    @Update("UPDATE app_project SET name = #{name}, description = #{description}, start = #{start}, finish = #{finish}, status = #{status} WHERE id = #{id}")
    void merge(@NotNull final Project entity) throws SQLException;

}
