package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndPoint {

    @NotNull
    @WebMethod
    List<Project> findAllProjects(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    Project findOneProject(@Nullable final String token, @NotNull final String id) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findProjectsByUserId(@Nullable final String token) throws Exception;

    @WebMethod
    void removeAllProjects(@Nullable final String token) throws Exception;

    @WebMethod
    void removeProjectsByUserId(@Nullable final String token) throws Exception;

    @WebMethod
    void removeOneProjectByUserId(@Nullable final String token, @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    Project persistProject(@Nullable final String token, @Nullable final Project p) throws Exception;

    @Nullable
    @WebMethod
    Project mergeProject(@Nullable final String token, @NotNull final Project p) throws Exception;

    @NotNull
    @WebMethod
    String getProjectId(@Nullable final String token, @NotNull final String value) throws Exception;

    @NotNull
    List<Project> findProjectsByNameOrDescription(@Nullable final String token, @NotNull final String value) throws Exception;

}
