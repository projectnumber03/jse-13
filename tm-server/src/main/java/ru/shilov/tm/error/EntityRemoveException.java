package ru.shilov.tm.error;

import org.jetbrains.annotations.NotNull;

public final class EntityRemoveException extends RuntimeException {

    public EntityRemoveException() {
        super("Ошибка удаления объекта");
    }

    public EntityRemoveException(@NotNull final Throwable throwable) {
        super("Ошибка удаления объекта", throwable);
    }

}
