package ru.shilov.tm.context;

import lombok.Getter;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.*;
import ru.shilov.tm.api.repository.*;
import ru.shilov.tm.api.service.*;
import ru.shilov.tm.endpoint.*;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.InitializationException;
import ru.shilov.tm.repository.SettingRepositoryImpl;
import ru.shilov.tm.service.*;
import ru.shilov.tm.util.SignatureUtil;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.util.Scanner;

public final class Bootstrap {

    @NotNull
    private final ISettingRepository settingRepo = new SettingRepositoryImpl();

    @Getter
    @NotNull
    private final ISettingService settingService = new SettingServiceImpl(settingRepo);

    @NotNull
    private final IProjectService projectService = ProjectServiceImpl.builder().bootstrap(this).build();

    @NotNull
    private final ITaskService taskService = TaskServiceImpl.builder().bootstrap(this).build();

    @Getter
    @NotNull
    private final IUserService userService = UserServiceImpl.builder().bootstrap(this).build();

    @NotNull
    private final ISessionService sessionService = SessionServiceImpl.builder().bootstrap(this).build();

    @NotNull
    private final IDataTransportService dataTransportService = new DataTrasportServiceImpl(projectService, taskService, userService);

    @NotNull
    private final IProjectEndPoint projectEndpoint = new ProjectEndPointImpl(projectService, sessionService);

    @NotNull
    private final ITaskEndPoint taskEndpoint = new TaskEndPointImpl(taskService, sessionService);

    @NotNull
    private final IUserEndPoint userEndpoint = new UserEndPointImpl(userService, sessionService);

    @NotNull
    private final ISessionEndPoint sessionEndpoint = new SessionEndPointImpl(sessionService);

    @NotNull
    private final IDataTransportEndPoint dataTransportEndPoint = new DataTransportEndPointImpl(dataTransportService, sessionService);

    @Getter
    @NotNull
    private final SqlSessionFactory sqlSessionFactory = initSqlSessionFactory();

    public void init() throws Exception {
        System.setProperty("javax.xml.bind.JAXBContextFactory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        new File("./saved").mkdirs();
        initEntities();
        @NotNull final String hostName = settingService.getProperty("hostname");
        @NotNull final String port = settingService.getProperty("port");
        Endpoint.publish(String.format("http://%s:%s/projectservice?wsdl", hostName, port), projectEndpoint);
        Endpoint.publish(String.format("http://%s:%s/taskservice?wsdl", hostName, port), taskEndpoint);
        Endpoint.publish(String.format("http://%s:%s/userservice?wsdl", hostName, port), userEndpoint);
        Endpoint.publish(String.format("http://%s:%s/sessionservice?wsdl", hostName, port), sessionEndpoint);
        Endpoint.publish(String.format("http://%s:%s/datatransportservice?wsdl", hostName, port), dataTransportEndPoint);
        @NotNull final Scanner scanner = new Scanner(System.in);
        while (!scanner.nextLine().equals("exit")) { }
        System.exit(0);
    }

    private void initEntities() throws Exception {
        try {
            @NotNull final String salt = settingService.getProperty("salt");
            @NotNull final String cycle = settingService.getProperty("cycle");
            taskService.removeAll();
            projectService.removeAll();
            sessionService.removeAll();
            userService.removeAll();
            userService.persist(new User("user", SignatureUtil.sign("123", salt, Integer.parseInt(cycle)), User.Role.USER));
            userService.persist(new User("admin", SignatureUtil.sign("123", salt, Integer.parseInt(cycle)), User.Role.ADMIN));
        } catch (EntityPersistException e) {
            throw new InitializationException();
        }
    }

    @NotNull
    private SqlSessionFactory initSqlSessionFactory() {
        @Nullable final String login = settingService.getProperty("db.login");
        @Nullable final String url = settingService.getProperty("db.url");
        @Nullable final String password = settingService.getProperty("db.password");
        @Nullable final String driver = settingService.getProperty("db.driver");
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, login, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
