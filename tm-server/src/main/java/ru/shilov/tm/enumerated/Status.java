package ru.shilov.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum Status {

    PLANNED("Запланировано"),
    IN_PROCESS("В процессе"),
    DONE("Готово");

    @NotNull
    private final String description;

}
