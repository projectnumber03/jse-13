package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.ITaskEndPoint;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.ITaskEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class TaskEndPointImpl implements ITaskEndPoint {

    @NotNull
    private ITaskService taskService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    @Override
    public List<Task> findAllTasks(@Nullable final String token) throws Exception {
        sessionService.validate(token);
        return taskService.findAll();
    }

    @NotNull
    @Override
    public Task findOneTask(@Nullable final String token, @NotNull final String id) throws Exception {
        sessionService.validate(token);
        return taskService.findOne(id);
    }

    @NotNull
    @Override
    public List<Task> findTasksByUserId(@Nullable final String token) throws Exception {
        return taskService.findByUserId(sessionService.validate(token).getUserId());
    }

    @NotNull
    @Override
    public List<Task> findTasksByProjectId(@Nullable final String token, @NotNull final String id) throws Exception {
        return taskService.findByProjectId(sessionService.validate(token).getUserId(), id);
    }

    @Override
    public void removeAllTasks(@Nullable final String token) throws Exception {
        sessionService.validate(token, Arrays.asList(User.Role.ADMIN));
        taskService.removeAll();
    }

    @Override
    public void removeTasksByUserId(@Nullable final String token) throws Exception {
        taskService.removeByUserId(sessionService.validate(token).getUserId());
    }

    @Override
    public void removeOneTaskByUserId(@Nullable final String token, @Nullable final String id) throws Exception {
        taskService.removeOneByUserId(sessionService.validate(token).getUserId(), id);
    }

    @Nullable
    @Override
    public Task persistTask(@Nullable final String token, @Nullable final Task t) throws Exception {
        @NotNull final Session currentSession = sessionService.validate(token);
        if (t != null) t.setUserId(currentSession.getUserId());
        return taskService.persist(t);
    }

    @Nullable
    @Override
    public Task mergeTask(@Nullable final String token, @NotNull final Task t) throws Exception {
        sessionService.validate(token);
        return taskService.merge(t);
    }

    @NotNull
    @Override
    public String getTaskId(@Nullable final String token, @NotNull final String value) throws Exception {
        return taskService.getId(sessionService.validate(token).getUserId(), value);
    }

    @NotNull
    @Override
    public List<Task> findTasksByNameOrDescription(@Nullable final String token, @NotNull final String value) throws Exception {
        return taskService.findByNameOrDescription(sessionService.validate(token).getUserId(), value);
    }

}
