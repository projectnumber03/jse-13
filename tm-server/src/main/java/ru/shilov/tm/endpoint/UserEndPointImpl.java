package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.IUserEndPoint;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.IUserEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class UserEndPointImpl implements IUserEndPoint {

    @NotNull
    private IUserService userService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    @Override
    public List<User> findAllUsers(@Nullable final String token) throws Exception {
        sessionService.validate(token);
        return userService.findAll();
    }

    @NotNull
    @Override
    public User findOneUser(@Nullable final String token) throws Exception {
        @NotNull final Session currentSession = sessionService.validate(token);
        return userService.findOne(currentSession.getUserId());
    }

    @Override
    public @NotNull User findOneUserById(@Nullable final String token, @Nullable final String userId) throws Exception {
        sessionService.validate(token);
        return userService.findOne(userId);
    }

    @Override
    public void removeAllUsers(@Nullable final String token) throws Exception {
        sessionService.validate(token);
        userService.removeAll();
    }

    @Nullable
    @Override
    public User persistUser(@Nullable final String token, @Nullable final User u) throws Exception {
        sessionService.validate(token);
        return userService.persist(u);
    }

    @Nullable
    @Override
    public User mergeUser(@Nullable final String token, @NotNull final User u) throws Exception {
        sessionService.validate(token);
        return userService.merge(u);
    }

    @Override
    public void removeOneUserById(@Nullable final String token, @Nullable final String id) throws Exception {
        sessionService.validate(token, Arrays.asList(User.Role.ADMIN));
        userService.removeOneById(id);
    }

    @NotNull
    @Override
    public String getUserId(@Nullable final String token, @NotNull final String value) throws Exception {
        sessionService.validate(token);
        return userService.getId(value);
    }

    @Nullable
    @Override
    public User registerUser(@Nullable final User u) {
        return userService.register(u);
    }

}
