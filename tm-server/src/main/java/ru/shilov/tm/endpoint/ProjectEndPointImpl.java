package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.IProjectEndPoint;
import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.IProjectEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectEndPointImpl implements IProjectEndPoint {

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjects(@Nullable final String session) throws Exception {
        sessionService.validate(session);
        return projectService.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public Project findOneProject(@Nullable final String session, @NotNull final String id) throws Exception {
        sessionService.validate(session);
        return projectService.findOne(id);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findProjectsByUserId(@Nullable final String session) throws Exception {
        return projectService.findByUserId(sessionService.validate(session).getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(@Nullable final String session) throws Exception {
        sessionService.validate(session, Arrays.asList(User.Role.ADMIN));
        projectService.removeAll();
    }

    @Override
    public void removeProjectsByUserId(@Nullable final String session) throws Exception {
        projectService.removeByUserId(sessionService.validate(session).getUserId());
    }

    @Override
    public void removeOneProjectByUserId(@Nullable final String session, @Nullable final String id) throws Exception {
        projectService.removeOneByUserId(sessionService.validate(session).getUserId(), id);
    }

    @Nullable
    @Override
    public Project persistProject(@Nullable final String session, @Nullable final Project p) throws Exception {
        @NotNull final Session currentSession = sessionService.validate(session);
        if (p != null) p.setUserId(currentSession.getUserId());
        return projectService.persist(p);
    }

    @Nullable
    @Override
    public Project mergeProject(@Nullable final String session, @NotNull final Project p) throws Exception {
        sessionService.validate(session);
        return projectService.merge(p);
    }

    @NotNull
    @Override
    public String getProjectId(@Nullable final String session, @NotNull final String value) throws Exception {
        return projectService.getId(sessionService.validate(session).getUserId(), value);
    }

    @NotNull
    @Override
    public List<Project> findProjectsByNameOrDescription(@Nullable final String session, @NotNull final String value) throws Exception {
        return projectService.findByNameOrDescription(sessionService.validate(session).getUserId(), value);
    }

}
