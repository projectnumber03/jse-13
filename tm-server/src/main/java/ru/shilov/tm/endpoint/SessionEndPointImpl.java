package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.ISessionEndPoint;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.ISessionEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class SessionEndPointImpl implements ISessionEndPoint {

    @NotNull
    private ISessionService sessionService;

    @Nullable
    @WebMethod
    public String createSession(@NotNull final String login, @NotNull final String password) throws Exception {
        return sessionService.create(login, password);
    }

    @Override
    @WebMethod
    public void removeSession(@Nullable final String token) throws Exception {
        @NotNull final Session session = sessionService.validate(token);
        sessionService.removeByUserId(session.getUserId());
    }

}
