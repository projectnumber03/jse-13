package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@SuperBuilder
public final class ProjectServiceImpl extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Override
    public List<Project> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(IProjectRepository.class).findByUserId(userId);
        } catch (SQLException e) {
            throw new NoSuchEntityException(e);
        }
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDescription(@Nullable final String userId, @Nullable final String value) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(value)) throw new NoSuchEntityException();
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(IProjectRepository.class).findByNameOrDescription(userId, String.format("%%%s%%", value));
        } catch (SQLException e) {
            throw new NoSuchEntityException(e);
        }
    }

    @Override
    public void removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            sqlSession.getMapper(IProjectRepository.class).removeByUserId(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            throw new EntityRemoveException(e);
        }
    }

    @Override
    public void removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final List<Task> projectTasks = taskRepository.findByProjectId(userId, id);
            for (@NotNull final String taskId : projectTasks.stream().map(Task::getId).collect(Collectors.toList())) {
                taskRepository.removeOneByUserId(userId, taskId);
            }
            sqlSession.getMapper(IProjectRepository.class).removeOneByUserId(userId, id);
            sqlSession.commit();
        } catch (SQLException e) {
            throw new EntityRemoveException(e);
        }
    }

    @NotNull
    @Override
    public String getId(@Nullable final String userId, @Nullable final String value) throws NumberToIdTransformException {
        if (Strings.isNullOrEmpty(value) || !isNumber(value) || Strings.isNullOrEmpty(userId))
            throw new NumberToIdTransformException();
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(IProjectRepository.class).getId(userId, Integer.parseInt(value) - 1);
        } catch (SQLException e) {
            throw new NumberToIdTransformException(value, e);
        }
    }

    @Override
    public Class<? extends IRepository<Project>> getMapperClass() {
        return IProjectRepository.class;
    }

}
