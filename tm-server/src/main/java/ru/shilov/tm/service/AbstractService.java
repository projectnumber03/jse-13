package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.service.IService;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.AbstractEntity;
import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;

import java.sql.SQLException;
import java.util.List;

@SuperBuilder
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    protected Bootstrap bootstrap;

    @NotNull
    @Override
    public List<T> findAll() {
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(getMapperClass()).findAll();
        } catch (SQLException e) {
            throw new NoSuchEntityException(e);
        }
    }

    @NotNull
    @Override
    public T findOne(@Nullable final String id) throws RuntimeException {
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            @Nullable final T entity;
            if (Strings.isNullOrEmpty(id) || (entity = sqlSession.getMapper(getMapperClass()).findOne(id)) == null) throw new NoSuchEntityException();
            return entity;
        } catch (SQLException e) {
            throw new NoSuchEntityException(e);
        }
    }

    @Override
    public void removeAll() {
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            sqlSession.getMapper(getMapperClass()).removeAll();
            sqlSession.commit();
        } catch (SQLException e) {
            throw new EntityRemoveException(e);
        }
    }

    @Nullable
    @Override
    public T persist(@Nullable final T entity) {
        if (entity == null) throw new EntityPersistException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            sqlSession.getMapper(getMapperClass()).persist(entity);
            sqlSession.commit();
            return entity;
        } catch (SQLException e) {
            throw new EntityPersistException(e);
        }
    }

    @Nullable
    @Override
    public T merge(@Nullable final T entity) {
        if (entity == null) throw new EntityMergeException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            sqlSession.getMapper(getMapperClass()).merge(entity);
            sqlSession.commit();
            return entity;
        } catch (SQLException e) {
            throw new EntityMergeException(e);
        }
    }

    @NotNull
    protected SqlSession openSession(@NotNull final Boolean autoCommit) {
        return bootstrap.getSqlSessionFactory().openSession(autoCommit);
    }

    protected boolean isNumber(@NotNull final String value) {
        return value.matches("\\d+") && Integer.parseInt(value) > 0;
    }

    public abstract Class<? extends IRepository<T>> getMapperClass();

}
