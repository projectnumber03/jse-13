package ru.shilov.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.repository.ISessionRepository;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;
import ru.shilov.tm.util.AES;
import ru.shilov.tm.util.SignatureUtil;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@SuperBuilder
public final class SessionServiceImpl extends AbstractService<Session> implements ISessionService {

    @Override
    public void removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            sqlSession.getMapper(ISessionRepository.class).removeByUserId(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            throw new EntityRemoveException();
        }
    }

    @Nullable
    @Override
    public Session findOneByUserId(@Nullable final String id, @Nullable final String userId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(ISessionRepository.class).findOneByUserId(userId, id);
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @Nullable
    public String create(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable final User user = bootstrap.getUserService().findByLogin(login);
        @Nullable final String salt = bootstrap.getSettingService().getProperty("salt");
        @Nullable final Integer cycle = Integer.parseInt(bootstrap.getSettingService().getProperty("cycle"));
        if(user.getPassword() != null && !user.getPassword().equals(SignatureUtil.sign(password, salt, cycle))) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session, salt, cycle));
        persist(session);
        @NotNull final String key = bootstrap.getSettingService().getProperty("decryption.key");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return AES.encrypt(json, key);
    }

    @NotNull
    @Override
    public Session validate(@Nullable final String sessionToValidate, @NotNull final List<User.Role> roles) throws Exception {
        if (sessionToValidate == null) throw new PermissionException();
        @NotNull final Session userSesstion = decrypt(sessionToValidate);
        if (!roles.contains(userSesstion.getRole())) throw new PermissionException();
        return validate(sessionToValidate);
    }

    @NotNull
    @Override
    public Session validate(@Nullable final String sessionToValidate) throws Exception {
        if (sessionToValidate == null) throw new PermissionException();
        @NotNull final Session userSession = decrypt(sessionToValidate);
        @Nullable final Session serverSession = findOneByUserId(userSession.getId(), userSession.getUserId());
        if (serverSession == null) throw new NoSuchSessionException();
        userSession.setSignature(null);
        @Nullable final String salt = bootstrap.getSettingService().getProperty("salt");
        @Nullable final Integer cycle = Integer.parseInt(bootstrap.getSettingService().getProperty("cycle"));
        if (serverSession.getSignature() != null && !serverSession.getSignature().equals(SignatureUtil.sign(userSession, salt, cycle))) throw new IllegalSessionException();
        if (ChronoUnit.SECONDS.between(userSession.getCreationDate(), LocalDateTime.now()) > 90) {
            removeByUserId(userSession.getUserId());
            throw new SessionTimeOutException();
        }
        return userSession;
    }

    private Session decrypt(@Nullable final String cryptSession) throws Exception {
        @NotNull final String key = bootstrap.getSettingService().getProperty("decryption.key");
        @NotNull final String json = AES.decrypt(cryptSession, key);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Session session = mapper.readValue(json, Session.class);
        return session;
    }

    @Override
    public Class<? extends IRepository<Session>> getMapperClass() {
        return ISessionRepository.class;
    }

}
