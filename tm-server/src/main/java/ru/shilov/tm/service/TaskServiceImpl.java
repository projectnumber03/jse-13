package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.sql.SQLException;
import java.util.List;

@SuperBuilder
public final class TaskServiceImpl extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Override
    public List<Task> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(ITaskRepository.class).findByUserId(userId);
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @Override
    public void removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            sqlSession.getMapper(ITaskRepository.class).removeByUserId(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            throw new EntityRemoveException();
        }
    }

    @NotNull
    @Override
    public List<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(projectId)) throw new NoSuchEntityException();
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(ITaskRepository.class).findByProjectId(userId, projectId);
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDescription(@Nullable String userId, @Nullable String value) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(value)) throw new NoSuchEntityException();
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(ITaskRepository.class).findByNameOrDescription(userId, String.format("%%%s%%", value));
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @Override
    public void removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            sqlSession.getMapper(ITaskRepository.class).removeOneByUserId(userId, id);
            sqlSession.commit();
        } catch (SQLException e) {
            throw new EntityRemoveException();
        }
    }

    @NotNull
    @Override
    public String getId(@Nullable final String userId, @Nullable final String value) throws NumberToIdTransformException {
        if (Strings.isNullOrEmpty(value) || !isNumber(value) || Strings.isNullOrEmpty(userId)) throw new NumberToIdTransformException(value);
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(ITaskRepository.class).getId(userId, Integer.parseInt(value) - 1);
        } catch (SQLException e) {
            throw new NumberToIdTransformException(value);
        }
    }

    @Override
    public Class<? extends IRepository<Task>> getMapperClass() {
        return ITaskRepository.class;
    }

}
