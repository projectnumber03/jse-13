package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.repository.ISessionRepository;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.ISettingService;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;
import ru.shilov.tm.util.SignatureUtil;

import java.sql.SQLException;

@SuperBuilder
public final class UserServiceImpl extends AbstractService<User> implements IUserService {

    @Override
    public void removeOneById(@Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id)) throw new EntityRemoveException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            sqlSession.getMapper(ISessionRepository.class).removeByUserId(id);
            sqlSession.getMapper(IUserRepository.class).removeOneById(id);
            sqlSession.commit();
        } catch (SQLException e) {
            throw new EntityRemoveException();
        }
    }

    @NotNull
    @Override
    public User register(@Nullable final User user) {
        if (user == null) throw new EntityPersistException();
        try (@NotNull final SqlSession sqlSession = openSession(false)) {
            @NotNull final String salt = bootstrap.getSettingService().getProperty("salt");
            @NotNull final String cycle = bootstrap.getSettingService().getProperty("cycle");
            user.setPassword(SignatureUtil.sign(user.getPassword(), salt, Integer.parseInt(cycle)));
            sqlSession.getMapper(IUserRepository.class).persist(user);
            sqlSession.commit();
            return user;
        } catch (SQLException e) {
            throw new EntityPersistException();
        }
    }

    @Nullable
    @Override
    public User merge(@Nullable final User user) throws RuntimeException {
        if (user == null) throw new EntityMergeException();
        @NotNull final String salt = bootstrap.getSettingService().getProperty("salt");
        @NotNull final String cycle = bootstrap.getSettingService().getProperty("cycle");
        user.setPassword(SignatureUtil.sign(user.getPassword(), salt, Integer.parseInt(cycle)));
        return super.merge(user);
    }

    @NotNull
    @Override
    public String getId(@Nullable final String value) throws NumberToIdTransformException {
        if (Strings.isNullOrEmpty(value) || !isNumber(value)) throw new NumberToIdTransformException();
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            return sqlSession.getMapper(IUserRepository.class).getId(Integer.parseInt(value) - 1);
        } catch (SQLException e) {
            throw new NumberToIdTransformException(value);
        }
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws NoSuchEntityException {
        try (@NotNull final SqlSession sqlSession = openSession(true)) {
            @Nullable final User user;
            if (Strings.isNullOrEmpty(login) || ((user = sqlSession.getMapper(IUserRepository.class).findByLogin(login)) == null)){
                throw new NoSuchEntityException();
            }
            return user;
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @Override
    public Class<? extends IRepository<User>> getMapperClass() {
        return IUserRepository.class;
    }

}
