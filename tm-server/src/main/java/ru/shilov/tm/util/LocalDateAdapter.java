package ru.shilov.tm.util;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    @NotNull
    @Override
    public LocalDate unmarshal(@NotNull final String s) {
        return LocalDate.parse(s, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    @NotNull
    @Override
    public String marshal(@NotNull final LocalDate localDate) {
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

}
