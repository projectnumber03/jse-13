package ru.shilov.tm.util;

import com.google.common.base.Strings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.error.DecryptException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public final class AES {

    @Nullable
    private static SecretKeySpec secretKey;

    private static void setKey(@NotNull final String myKey) throws Exception {
        @Nullable MessageDigest sha;
        byte[] key = myKey.getBytes(StandardCharsets.UTF_8);
        sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        secretKey = new SecretKeySpec(key, "AES");
    }

    @NotNull
    public static String encrypt(@NotNull final String strToEncrypt, @NotNull final String secret) throws Exception {
        setKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        @NotNull final byte[] data = cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(data);
    }

    @NotNull
    public static String decrypt(@Nullable final String strToDecrypt, @Nullable final String secret) throws Exception {
        if (Strings.isNullOrEmpty(strToDecrypt) || Strings.isNullOrEmpty(secret)) throw new DecryptException();
        setKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        @NotNull final byte[] data = Base64.getDecoder().decode(strToDecrypt);
        return new String(cipher.doFinal(data));
    }

}
